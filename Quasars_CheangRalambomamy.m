% 0/ On charge les fichiers : 46420 donn�es et 22 variables %
% Import de la matrice X des donn�es%
X = load('P:\ADD\quasar_X.txt')
 
% Import des noms de variables%
Variables = importdata('P:\ADD\quasar_variables.txt')

% Import des noms de quasars%
Quasars = importdata('P:\ADD\quasar_names.txt')

% 1/ Calculons la moyenne et l'�cart-type pour chaque variables %
E = mean(X)
ST = std(X)

% 2/ ACP centr�e : on va centr�e les variables %
Xc = X - repmat(E, 46420, 1)

% 3/ ACP r�duite : on va r�duire les variables => variances � 1
Xr = X ./ repmat(ST, 46420, 1)

% 4/ ACP standardis�e ou normalis�e : on standardise = centree + reduit %
Xs = Xc ./ repmat(ST, 46420, 1)

% 5/ Recherche de la matrice variance-covariance%
V = transpose(Xs)*(Xs) / (46420-1)
% Matrice de corr�lation : V = corr(X)%
% On remarque que la matrice de corr�lation de X est �gal � la matrice de
% variance-covariance de Xs %
% D'apr�s la matrice de corr�lation, les variables semblent : %
% 6 et 3 : corr�lation n�gative %
% 8 et 7, 9 et 7, 11 et 7, 11 et 9, 13 et 9, 15 et 9, 13 et 11, 15 et 11, 16 et 12, 15 et 13, 16 et 14 : corr�lation %
% 17 et 18 et 19 et 21 et 22 : tr�s bonne corr�lation 2 � 2 %

% 6/ Recherche des valeurs propres et des vecteurs propres %
[EVECT,EVAL] = eig(V)

% 7/ Les variables ou facteurs � retenir %
% Extraction de la diagonale%
VAL_PROPRE = diag(EVAL)
SUM_VAL_PROPRE = sum(VAL_PROPRE)
%On obtient bien que cest egal au nombre de variables%

%Calcul du pourcentage%
PERCENT = (VAL_PROPRE ./ repmat(SUM_VAL_PROPRE,22,1))*100

%Calcul du pourcentage cumul� (inertie cumul� des valeurs propres)%
CUMUL_PERCENT = cumsum(PERCENT)
%On va retenir les 4 premiers facteurs %
%car elles contiennent plus de 70 pourcent de l'information (inertie totale)%

% 8/ Graphique des valeurs propres%
bar(EVAL)
%Le graphique permet de dire que l'on peut retenir les 4 premi�res
%composantes%

% 9/ Standardisation des vecteurs propres pour l'analyse des donn�es %
F = EVECT * (EVAL^(1/2))

% 10 / Calcul de la matrice de projection P des quasars pour l'analyse des donn�es%
P = Xs * EVECT * (EVAL^(-1/2))

% 11/ Cercle de corr�lation des variables et nuage de points%

% Premier plan factoriel
nuage = plot(P(:,1),P(:,2), '.')
set(nuage, 'Color', 'red')
hold on
angle = linspace(0, 2*pi, 360)
x = cos(angle)
y = sin(angle)
plot(x, y)
axis('equal')
hold on
a = [-1;1]
b = [0 ;0]
c = [1;-1]
plot(a, b)
plot(b,c)
text(1,0, 'Axe P1 : 37.21%')
text(0,1, 'Axe P2 : 15.07%')
hold on
for i=1 : length(F)
    O = [0 ;F(i,1)]
    x1 = [0;F(i,2)]
    plot(O, x1)
    text(F(:,1),F(:,2), Variables)
end
hold off

% Deuxi�me plan factoriel
nuage = plot(P(:,1),P(:,3), '.')
set(nuage, 'Color', 'red')
hold on
angle = linspace(0, 2*pi, 360)
x = cos(angle)
y = sin(angle)
plot(x, y)
axis('equal')
hold on
a = [-1;1]
b = [0 ;0]
c = [1;-1]
plot(a, b)
plot(b,c)
text(1,0, 'Axe P1 : 37.21%')
text(0,1, 'Axe P3 : 10.55%')
hold on
for i=1 : length(F)
    O = [0 ;F(i,1)]
    x1 = [0;F(i,3)]
    plot(O, x1)
    text(F(:,1),F(:,3), Variables)
end
hold off

% Troisi�me plan factoriel
nuage = plot(P(:,1),P(:,4), '.')
set(nuage, 'Color', 'red')
hold on
angle = linspace(0, 2*pi, 360)
x = cos(angle)
y = sin(angle)
plot(x, y)
axis('equal')
hold on
a = [-1;1]
b = [0 ;0]
c = [1;-1]
plot(a, b)
plot(b,c)
text(1,0, 'Axe P1 : 37.21%')
text(0,1, 'Axe P4 : 9.01%')
hold on
for i=1 : length(F)
    O = [0 ;F(i,1)]
    x1 = [0;F(i,4)]
    plot(O, x1)
    text(F(:,1),F(:,4), Variables)
end
hold off

% Nuage de points
axis('equal')
nuage = plot(P(:,1),P(:,2), '.')
set(nuage, 'Color', 'red')

axis('equal')
nuage = plot(P(:,1),P(:,3), '.')
set(nuage, 'Color', 'red')

axis('equal')
nuage = plot(P(:,1),P(:,4), '.')
set(nuage, 'Color', 'red')